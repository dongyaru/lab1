#include <stdio.h>
#include <stdlib.h> 
#include <omp.h>

int threads;
// merge sort function

// function to merge the subarrays
void merges(int a[], int p, int q, int r)
{
    int b[r+1];   //same size of a[]
    int i, j, k;
    k = 0;
    i = p;
    j = q + 1;
    while(i <= q && j <= r)
    {
        if(a[i] < a[j])
        {
            b[k++] = a[i++];    // same as b[k]=a[i]; k++; i++;
        }
        else
        {
            b[k++] = a[j++];
        }
    }
  
    while(i <= q)
    {
        b[k++] = a[i++];
    }
  
    while(j <= r)
    {
        b[k++] = a[j++];
    }
  
    for(i=r; i >= p; i--)
    {
        a[i] = b[--k];  // copying back the sorted list to a[]
    } 
}
void mergeSort(int a[], int p, int r)
{
    int q;
	
    if(p < r)
    {
        q = (p + r) / 2;
		#pragma omp parallel sections
		{
		#pragma omp section
        mergeSort(a, p, q);
		#pragma omp section
        mergeSort(a, q+1, r);
		#pragma omp section
        merges(a, p, q, r);
		}
    }
}
 
 
// function to print the array
void printArray(int a[], int size)
{
    int i;
    for (i=0; i < size; i++)
    {
        printf("%d ", a[i]);
    }
    printf("\n");
}
 
int main(int argc,char **argv)
{
	int len,i;
	printf("array's length: ");
	scanf("%d",&len);
    int arr[len];
	threads=1;
	printf("threads: ");
	scanf("%d",&threads);
	for(i=0;i<len;i++)
        arr[i]=rand()%300+12;
    printf("Given array: \n");
    printArray(arr, len);
    omp_set_num_threads(threads);
	double t1=omp_get_wtime();
    // calling merge sort
    mergeSort(arr, 0, len - 1);
	double t2=omp_get_wtime();
	
    printf("\nSorted array: \n");
    printArray(arr, len);
	printf("Execute time= %f\n",t2-t1);
    return 0;
}