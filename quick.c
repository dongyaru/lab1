#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
int threads;
void QuickSort(int *arr, int low, int high)
{
    if (low < high)
    {
        int i = low;
        int j = high;
        int k = arr[low];
        while (i < j)
        {
            while(i < j && arr[j] >= k)    
            {
                j--;
            }
 
            if(i < j)
            {
                arr[i++] = arr[j];
            }
 
            while(i < j && arr[i] < k)      
            {
                i++;
            }
 
            if(i < j)
            {
                arr[j--] = arr[i];
            }
        }
 
        arr[i] = k;
 
        // 
		#pragma omp parallel sections
		{
		#pragma omp section
        QuickSort(arr, low, i - 1);     
		#pragma omp section
        QuickSort(arr, i + 1, high);
		}
    }
}

int main()
{
	int len;
	printf("array's length: ");
	scanf("%d",&len);
    int a[len], i;    
	printf("threads: ");
	scanf("%d",&threads);
    printf("Given array: \n");
    for(i=0;i<len;i++){
        a[i]=rand()%300+12;
	    printf(" %d ",a[i]);   
	}
	printf("\n");
	omp_set_num_threads(threads);
	double t1=omp_get_wtime();
    QuickSort(a, 0, len-1);
	double t2=omp_get_wtime();
  printf("\n");
  printf("\nSorted array: \n");
    for(i=0;i<len;i++)
        printf(" %d ",a[i]);    //
    printf("\n");
	printf("Execute time= %f\n",t2-t1);
    return 0;
}
